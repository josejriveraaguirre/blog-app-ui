import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/models/Post';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.css']
})
export class ListPostsComponent implements OnInit {

  posts: Post[] = [];

  constructor(private postSvc: PostService) { }

  ngOnInit(): void {
    this.listPosts();
  }

  // method that will display the list of posts/data
  listPosts() {
  this.postSvc.getPosts().subscribe(
    data => this.posts = data
    )
  }

  // method to delete posts using the method from our service

  deletedPost(id: number) {
    this.postSvc.deletePost(id).subscribe(
      data => this.listPosts
    )
  }
}

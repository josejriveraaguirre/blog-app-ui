import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../models/Post';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  // assigning url of backend controller

  private getUrl: string = "http://localhost:8080/api/v1/posts";

// using dependency injection to bring the 

  constructor(private httpClient: HttpClient) { }

  // method to request to the get mapping method on back end

  getPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(this.getUrl).pipe(
      map(result => result)
    )
  }

// method that will make a request to the PostMapping method in our backend
  savePost(newPost: Post): Observable<Post> {
    return this.httpClient.post<Post>(this.getUrl, newPost);
  }

  viewPost(id: number): Observable<Post> {
    return this.httpClient.get<Post>(`${this.getUrl}/${id}`).pipe(
      map(result => result)
    )
  }

  // method to delete a post
  deletePost(id: number) : Observable<any> {
    return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: 'text'})
  }


}
